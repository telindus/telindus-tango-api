# DigitalKYC API Code Samples

## Instructions
- Clone this repo
- In VSCode
    - open the workspace tango-api-samples.code-workspace
    - ensure the HTTP Client extension is installed
        * Directly in VSCode, search for HTTP Client
        or
        * VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=mkloubert.vscode-http-client
    - Setup the REST client to use tango.dev environment:
		press CTRL+SHIFT+P and type "Switch Environment"
	- You can now execute each call in src/Profiles.http by clicking on "Send Request" just over it